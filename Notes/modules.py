#!/usr/bin/env python
"""Typing module"""

import typing

__version__ = '1.0.0'
__author__ = 'Wieruchowski Arkadiusz'
__email__ = 'arkadiusz.wieruchowski@outlook.com'
__copyright__ = 'Copyright by Arkadiusz Wieruchowski'

class LivingCreature:
    """Living creature abstraction class"""
    pass

class Animal(LivingCreature):
    """Animal abstraction class"""
    pass

class Plant(LivingCreature):
    """Plant abstraction class"""
    pass

lc = LivingCreature()
animal = Animal()
plant = Plant()

def create_animal() -> Animal:
    """ Funcion every time create new object of class Animal"""
    return Animal()

dog = create_animal()
print(dog);


def print_info_about_obj(obj: typing.Union[Animal,Plant]) -> None:
    """Print info about obj instances"""
    print(isinstance(obj, LivingCreature), isinstance(obj, Animal), isinstance(obj, Plant))

print_info_about_obj(lc)
print_info_about_obj(animal)
print_info_about_obj(plant)