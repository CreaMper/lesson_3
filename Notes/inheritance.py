#!/usr/bin/env python
"""Inheritance explained """

__version__ = '1.0.0'
__author__ = 'Wieruchowski Arkadiusz'
__email__ = 'arkadiusz.wieruchowski@outlook.com'
__copyright__ = 'Copyright by Arkadiusz Wieruchowski'

class LivingCreature:
    """Living creature abstraction class"""
    pass

class Animal(LivingCreature):
    """Animal abstraction class"""
    pass

class Plant(LivingCreature):
    """Plant abstraction class"""
    pass


lc = LivingCreature()
animal = Animal()
plant = Plant()
print(lc, Animal, Plant)
print(isinstance(lc, LivingCreature), isinstance(lc, Animal), isinstance(lc, Plant))
print(isinstance(animal, LivingCreature), isinstance(animal, Animal), isinstance(animal, Plant))
print(isinstance(plant, LivingCreature), isinstance(plant, Animal), isinstance(plant, Plant))

print(isinstance(plant, object), isinstance(plant, object), isinstance(plant, object))
