#!/usr/bin/env python
"""De/Constructions and their usage"""

__version__ = '1.0.0'
__author__ = 'Wieruchowski Arkadiusz'
__email__ = 'arkadiusz.wieruchowski@outlook.com'
__copyright__ = 'Copyright by Arkadiusz Wieruchowski'

class LivingCreature:
    """Living creature abstraction class"""
    def __init__(self):
        """Contructor"""
        print("Living creature has been initialized")
        print(self)

    def __del__(self):
        """DEstructior"""
        print("do not destroy me!",self)

    def breathing(self):
        """Living creature need to breathing"""
        print(self, "I breathing")

class Animal(LivingCreature):
    """Animal abstraction class"""

    def __init__(self):
        """Contructor"""
        super(Animal, self).__init__()
        print("I'am an Animal")


class Plant(LivingCreature):
    """Plant abstraction class"""

    def breathing(self):
        """Plant also need to take a breath"""
        print("Em I using an photosinthesis?")
        super(Plant, self).breathing()

    def is_a_plant(self, type_from_user) -> bool:
        """Object is a given type instance
        :param type_from_user: Type for user
        :return: Boolen
        """
        return isinstance(self, type_from_user)

lc = LivingCreature()
animal = Animal()
plant = Plant()

lc.breathing()
animal.breathing()
plant.breathing()

print(plant.is_a_plant(str))