#!/usr/bin/env python
"""Building a real human!"""

__version__ = '1.0.0'
__author__ = 'Wieruchowski Arkadiusz'
__email__ = 'arkadiusz.wieruchowski@outlook.com'
__copyright__ = 'Copyright by Arkadiusz Wieruchowski'

class Human:
    """Human class set-up"""
    def __init__(self,name,surname,age):
        """Ctor"""
        self.name = name
        self.surname = surname
        self.age = age

    def __repr__(self) -> str:
        """Print representation"""
        return f"{self.name} {self.surname}"

class Programmer(Human):
    """Programmer class"""
    def __init__(self,programming_lang):
        """Ctor
        :param programming_lang : The most liked languae
        """
        super(Programmer, self).__init__(name, surname, age)
        self.programming_lang = programming_lang
