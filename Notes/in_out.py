#!/usr/bin/env python
"""Input and output method explained"""

__version__ = '1.0.0'
__author__ = 'Wieruchowski Arkadiusz'
__email__ = 'arkadiusz.wieruchowski@outlook.com'
__copyright__ = 'Copyright by Arkadiusz Wieruchowski'

import sys

print("STD.OUT")
print("SRD.ERR", file=sys.stderr)


name = input("Gimme ur name: ")
age = input("Gimme ur age: ")
print(name, int(age))