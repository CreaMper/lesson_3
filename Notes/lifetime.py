#!/usr/bin/env python
"""Lifetime explained"""

__version__ = '1.0.0'
__author__ = 'Wieruchowski Arkadiusz'
__email__ = 'arkadiusz.wieruchowski@outlook.com'
__copyright__ = 'Copyright by Arkadiusz Wieruchowski'

class B:
    def __init__(self):
        print("B is alive")

    def __del__(self):
        print("B is dead")

class A:
    def __init__(self):
        print("I am alive")

    def __del__(self):
        print("I am dead")

def example():
    b = B()
    print("Example func")

def create_b():
    return B()


b = create_b()
print("This is sparta")
