#!/usr/bin/env python
"""Homework #1 Creating a class tree structure (3 stages) """

__version__ = '1.0.0'
__author__ = 'Wieruchowski Arkadiusz'
__email__ = 'arkadiusz.wieruchowski@outlook.com'
__copyright__ = 'Copyright by Arkadiusz Wieruchowski'


class PC:
    """Living creature abstraction class"""
    price = int(4500)

    def show_pc_name(self):
        """Method that shows name of da PC"""
        print("Computer name is Killer Double shot Pro v2")


class Processor(PC):
    """Class that will hold vars form a parent class"""
    def __init__(self):
        """Show PC price please"""
        print("Pc price : ", PC.price)


class Motherboard(PC):
    """Class that will hold method from a parent class"""
    def __init__(self):
        PC.show_pc_name("Killer Double shot Pro v2")


class Ram(PC):
    """Class that will overflow parent method | + super"""

    def show_pc_name(self):
        """Method showing us both names"""
        print("da Computer namde is Killer Double shot Pro v2")
        super(Ram, self).show_pc_name()
