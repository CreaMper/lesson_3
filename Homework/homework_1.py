#!/usr/bin/env python
"""Homework #1 Creating a class tree structure (3 stages) """

__version__ = '1.0.0'
__author__ = 'Wieruchowski Arkadiusz'
__email__ = 'arkadiusz.wieruchowski@outlook.com'
__copyright__ = 'Copyright by Arkadiusz Wieruchowski'

class PC:
    """Living creature abstraction class"""
    pass

class Motherboard(PC):
    """Animal abstraction class"""
    pass

class Procesor(Motherboard):
    """Plant abstraction class"""
    pass
