#!/usr/bin/env python
"""Homework #1 Creating a class tree structure (3 stages) """

__version__ = '1.0.0'
__author__ = 'Wieruchowski Arkadiusz'
__email__ = 'arkadiusz.wieruchowski@outlook.com'
__copyright__ = 'Copyright by Arkadiusz Wieruchowski'


def print_my_name(name: str) -> None:
    """ Show name"""
    print("My name is", name)


print_my_name("Arek")


def sum_of_3(a: int, b: int, c: int) -> None:
    """ Add 3 numbers """
    print(a+b+c)


sum_of_3(3, 3, 2)


def hi_there() -> str:
    """Hello there function"""
    return "Hello There!"


print(hi_there())


def sub_of_2(a: int, b: int) -> None:
    """ Substract 2 numbers """
    print(a-b)


sub_of_2(3, 3)


class Mouse: pass


def create_mouse() -> Mouse:
    """ Function that creates ExampleObj"""
    return Mouse()


mickey = create_mouse()
print(isinstance(mickey, Mouse))






